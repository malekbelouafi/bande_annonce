<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    use HasFactory;

    protected $table = 'movie';

    protected $primaryKey = 'movie_id';

    public $timestamps = false;

    protected $fillable = [
        'a_mettre_a_jour',
    ];

    public function persones()
    {
    	return $this->belongsToMany(Person::class, 'movie_person','movie_movie_id','person_person_id')->distinct('person_person_id')->withPivot(['person_type_person_type_id','person_id_origine'])->using(MoviePersonPivot::class);
    }
}
