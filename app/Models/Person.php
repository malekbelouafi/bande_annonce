<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    use HasFactory;

    protected $table = 'person';

    protected $primaryKey = 'person_id';

    public $timestamps = false;

	protected $fillable = [
        'a_mettre_a_jour',
    ];

    public function movies()
    {
    	return $this->belongsToMany(Movie::class, 'movie_person','person_person_id','movie_movie_id')->distinct('movie_movie_id');
    }
}
