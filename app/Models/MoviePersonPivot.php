<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\Pivot;

class MoviePersonPivot extends Pivot
{
    use HasFactory;

    protected $table = 'movie_person';

    public function type()
    {
        return $this->hasOne(PersonType::class,'person_type_id', 'person_type_person_type_id');
    }
}
