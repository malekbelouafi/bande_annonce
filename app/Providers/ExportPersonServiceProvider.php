<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositorys\ScopedMovieRepository;
use App\Repositorys\MovieRepository;
use App\Jobs\ExportPersonJob;

class ExportPersonServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //bind de repository avec l'interface
        $this->app->bind(MovieRepository::class, function($app,$parameters)
        {
            return new ScopedMovieRepository($parameters['type']);
        });
        
        //bind de repository avec job
        $this->app->bindMethod(ExportPersonJob::class.'@handle', function ($job, $app) {
            return $job->handle($app->makeWith(MovieRepository::class,['type' => $job->type]));
        });
    }
}
