<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\MovieResource;

class PersonResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->pivot->person_id_origine,
            'idPerson' => $this->person_id,
            'suppression' => 0,
            'type' => $this->pivot->type->name_person_type,
            'content' => [
                'nom' => $this->name_person,
                'url_dbpedia' => $this->url_dbpedia,
                'lieu_naissance' => $this->lieu_naissance,
                'nationalite' => $this->nationalite,
                'commentaire' => $this->commentaire,
                'profession' => $this->profession,
                'date_naissance' => $this->date_naissance,
                'photo' => $this->photo
            ],
            'movies' => new MovieResource($this->movies)
        ];
    }
}
