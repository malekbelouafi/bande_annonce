<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class MovieResource extends ResourceCollection
{
    public $preserveKeys = true;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->mapWithKeys(function ($item) {
            return [ $item->movie_id => [
                    'title' => $item->original_title,
                    'content' => [
                        'brightcove_id' => $item->brightcove_id,
                        'product_title' => $item->product_title,
                        'age_limit' => $item->age_limit,
                        'description' => $item->description,
                        'movie_duration' => $item->movie_duration,
                        'imdb_id' => $item->imdb_id,
                        'original_title' => $item->original_title,
                        'premiere' => $item->premiere,
                        'production_year' => $item->production_year,
                        'search_engine' => $item->search_engine,
                        'official_website' =>$item->official_website
                        ]
                    ]
                ];             
        });
    }
}
