<?php

namespace App\Repositorys;

use App\Models\Movie;
use App\Http\Resources\PersonResource;
use Storage,DB,Cache;

class ScopedMovieRepository implements MovieRepository
{
    public $type; //type de fichier a créer

    private $ids_update_m; //liste des movies a modifier
    private $todo_cache_p; //colection des person a traiter en cache
    private $total_m; //max movie a traiter
    private $total_p; //max file a créer

    public function __construct($type)
    {
        $this->type = $type;
        self::int(); // initialisaiton de configuration 
    }

    public function export(): bool
    {
        try {
            DB::beginTransaction();
            //recuperer liste des movies Lazycollection
            $movies = Movie::where('a_mettre_a_jour', 1)
            ->With(['persones' => function($query){
                $query->whereNotNull('id_kwm_lp');
            }])
            ->whereHas('persones', function($query){
                $query->whereNotNull('id_kwm_lp');
            })
            ->cursor();

            //vérification liste n'est pas vide
            //et cache traité
            if(count($movies) && $this->execCache()){

                //Pour chaque movie dans la liste ...
                $movies->each(function($movie){
                    //condition d'arret
                    //Si le nombre des movies traités est 25
                    if($this->total_m == 0){
                        return false;
                    }
                    //Pour chaque person a traiter ...
                    $movie->persones->each(function($persone, $key) use($movie){
                        //Si le nombre des person traités est 10
                        //Ajout dans la collection de cache
                        if($this->total_p  == 0){
                            //1- couper la liste des person a partir de $key
                            //2- merge la collection avec la colelction global de cache 
                            //3- return false pour passer au movie suivant
                            $this->todo_cache_p =  $this->todo_cache_p->merge($movie->persones->slice($key));
                            return false;
                        }else{
                            //traitement de création de fichier
                            $this->storageFile($persone);
                            //décrémenter de compteur person traités
                            $this->total_p--;
                        }
                    });

                    //Movie traité donc ajout de l'id dans le tableau de modification
                    $this->ids_update_m[] = $movie->movie_id;
                    //décrémenter de compteur movie traités
                    $this->total_m--;
                });
            }

            //Ajout des person a traiter dans la cache
            //Si la collection n'est pas vide
            if($this->todo_cache_p->isNotEmpty())
            {
                Cache::forever('person_json', $this->todo_cache_p);
            }

            //Modification au niveau de la base des données avec une seule requete
            if(!empty($this->ids_update_m))
            {
                Movie::whereIn('movie_id',$this->ids_update_m)->update(['a_mettre_a_jour' => 0]);              
            }

            DB::commit();  
            return true;            
        } catch (Exception $e) {
            log::info('erreur ScopedMovieRepository '.$e->getMessage());
            DB::rollback();
            return false;  
        }
    }

    public function storageFile($persone) : void
    {
        //Mapping des données
        $data = new PersonResource($persone);
        //Creation de fichier Json sous le dossier storage\app\
        Storage::put($persone->person_id.'.'.$this->type, $data->toJson());
        //changer la statut de person traité
        $persone->update(['a_mettre_a_jour'=> 0]);
    }


    public function execCache() : bool
    {
        if(Cache::Has('person_json') && count(Cache::get('person_json')))
        {
            //récupération de cache
            $a_traiters = Cache::get('person_json');

            foreach ($a_traiters as $key => $person) {
                //condition d'arret
                //Si le nombre des person traités est 10
                if($this->total_p  == 0){
                    break;
                    return false; 
                }

                // creation de fichier
                $this->storageFile($person);
                unset($a_traiters[$key]);

                //décrémenter de compteur person traités
                $this->total_p--;
            }
            
            //mis a jour de cache
            Cache::forever('person_json', $a_traiters);
        }

        return true;
    }

    private function int() : void
    {
        $this->ids_update_m = [];
        $this->todo_cache_p = collect();
        $this->total_m = config('export.person.max_movie');  
        $this->total_p = config('export.person.max_file');   
    }
}