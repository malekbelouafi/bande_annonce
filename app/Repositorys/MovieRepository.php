<?php

namespace App\Repositorys;

interface MovieRepository
{
    public function export(): bool;
}