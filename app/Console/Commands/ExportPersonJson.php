<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\ExportPersonJob;

class ExportPersonJson extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'person:export {type : type des fichiers a sortir }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sortir les fiches JSON des personnes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            dispatch(new ExportPersonJob($this->argument('type')));
            $this->info('Exportaion json lancée en Job');
        } catch (\Exception $e) {
            $this->info('Erreur: '.$e->getMessage());
        }
        
    }
}
