<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Models\Movie;
use App\Repositorys\MovieRepository;
use Cache;

class CachePersonExportTest extends TestCase
{
    protected $movieRepository;
    protected $movies;

    protected function setUp(): void
    {
        parent::setUp();

        $this->movieRepository = $this->app->makeWith(MovieRepository::class,['type' => 'json']);


        $this->movies = Movie::whereIn('movie_id', [547,819]);
    }

    //données dans la base
    public function test_person_export_json_command()
    {
        //Total movies a traiter
        $count = $this->movies->count();
        $this->assertEquals(2, $count);


        // Total Persons
        $total_p = $this->movies->With(['persones' => function($query){
            $query->whereNotNull('id_kwm_lp');
        }])
        ->whereHas('persones', function($query){
            $query->whereNotNull('id_kwm_lp');
        })
        ->withCount('persones')
        ->get()
        ->reduce(function($total,$item){
            return $total+$item->persones_count;
        },0);


        $this->assertEquals(15, $total_p);
    }

    //données dans la base
    public function test_cache_after_first_exec()
    {
        //movies a jour
        $this->movies->update(['a_mettre_a_jour' => 1]); 
        $count = $this->movies->where('a_mettre_a_jour', 1)->count();
        $this->assertEquals(2, $count);

        //lancement de traitement
        $this->assertEquals($this->movieRepository->export(),true);

        //vérification cache 
        $this->assertTrue(Cache::has('person_json'));

        $cache = Cache::get('person_json');
        $this->assertTrue(count($cache) > 0);

        //traiter 10 donc le reste 5
        $this->assertEquals(count($cache),5);
    }

}
