<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Repositorys\ScopedMovieRepository;
use App\Repositorys\MovieRepository;

class ExportPersonServiceProvideTest extends TestCase
{
    protected $movieRepository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->movieRepository = $this->app->makeWith(MovieRepository::class,['type' => 'json']);
    }

    public function test_service_provider()
    {

        $this->assertInstanceOf(ScopedMovieRepository::class, $this->movieRepository);

        $this->assertEquals($this->movieRepository->export(),true);
        //$this->assertEquals(gettype($this->movieRepository->export()),'array');
    }
}
