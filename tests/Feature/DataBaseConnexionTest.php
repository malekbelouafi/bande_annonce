<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Person;
use App\Models\Movie;

class DataBaseConnexionTest extends TestCase
{

    public function test_check_models()
    {
        // check tables
        $persons = Person::all();
        $this->assertNotEquals($persons,0);

        $this->assertDatabaseHas('person', [
            'person_id' => '1'
        ]);

        $movies = Movie::all();
        $this->assertNotEquals($movies,0);

        $this->assertDatabaseHas('movie', [
            'movie_id' => '547'
        ]);
    }

    public function test_check_relations()
    {
        // check relations
        $movies = Movie::whereHas('persones')->get();
        $persons = Person::whereHas('movies')->get();
        
        $this->assertInstanceOf(Person::class, $movies->first()->persones->first());
        $this->assertInstanceOf(Movie::class, $persons->first()->movies->first());

    }
}
