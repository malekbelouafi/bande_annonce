<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Support\Facades\Queue;
use App\Jobs\ExportPersonJob;
use App\Repositorys\MovieRepository;

class PersonExportJobTest extends TestCase
{
    //lancement de job
    public function test_person_export_job()
    {
        Queue::fake();
        $type = "json";

        dispatch(new ExportPersonJob($type));

        Queue::assertPushed(ExportPersonJob::class, function ($job) {
            $job->handle($this->app->makeWith(MovieRepository::class,['type' => $type]));
            return ($job->result == true);
        });
    }
}
