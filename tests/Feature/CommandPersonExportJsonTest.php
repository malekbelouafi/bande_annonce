<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CommandPersonExportJsonTest extends TestCase
{
    //lancement de commande
    public function test_person_export_json_command()
    {
        $this->artisan('person:export json')
            ->expectsOutput('Exportaion json lancée en Job')
            ->doesntExpectOutput('')
            ->assertExitCode(0);
    }
}
