<?php

use Illuminate\Support\Facades\Route;
use App\Repositorys\MovieRepository;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	$MovieRepository = App()->makeWith(MovieRepository::class,['type' => 'json']);
	$result = $MovieRepository->export();
	var_dump($result); 
    //return view('welcome');
});
