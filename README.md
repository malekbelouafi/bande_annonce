## Installation

Pour faire l'installation après le **clone** de projet a partir de **Git**, il faut lancer les commande ci-dessous:

- `composer install`
- Lancer la commande `php artisan key:generate` au terminal \PATH_DOSSIER
- Fichier **.env**  mettre a jours les configuration ci-dessous;
    - `QUEUE_CONNECTION=database`
    - `DB_DATABASE= ****`
    - `DB_USERNAME= ****`
    - `DB_PASSWORD= ****`
- Lancer la commande `php artisan migrate`

## Exécution
Lancer ces différents commandes au terminal \PATH_DOSSIER dans des différents fenêtres
 
 1. **Lancer le système queue:**  `php artisan queue:work` 
 2. **Lancer le système Schedule:**  `php artisan schedule:work` 
 3. **Lancer la nouvelle commande CLI**:  `php artisan person:export json`

Possibilité de modifier le type des fichiers a exporter, exemple:
`php artisan person:export test`

Possibilité de modifier le nombre max des fichiers a exporter dans 
`\PATTH_DOSSIER\config\export.php`

Par défaut:

    'person' => [
    
        'max_movie' => 25,
    
        'max_file' => 10
    ]

  

## Test

Faire le test avec PHPUnit:

 - Lancer cette commande au terminal \PATH_DOSSIER
	 `php artisan test --filter CommandPersonExportJsonTest`

Faire le test avec le navigateur:

 - Accéder a l'url retourné `php artisan serve`  (exemple : http://127.0.0.1:8000/)


## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
